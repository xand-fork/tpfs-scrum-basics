# TPFS Engineering Scrum Basics

This project contains documentation which should be kept up to date by the Scrum leaders on Team Cookie Monster.  It contains markdown content and a build script which uses pandoc to turn that markdown into an additional HTML page that can be deployed as a Gitlab Pages webpage.

## License

This project is licensed under MIT OR Apache-2.0

## Viewing

The documentation can be viewed at https://transparentincdevelopment.gitlab.io/docs/tpfs-scrum-basics/ or https://scrum.xand.tools (TODO). 

## Modifying Content

All content modification can be made as markdown directly to the guides in [content](/content).  Make sure to create MRs for a proper review and historical changeset.

## Deploying

The CI script automatically runs `build.sh` to run pandoc on the markdown content and create HTML content from it.  It then deploys this per the [Gitlab Pages instructions](https://docs.gitlab.com/ee/user/project/pages/).

## DNS Settings (TODO)

To make the content available at `scrum.xand.tools`, that is managed at the DNS level with our google dns.  Because our gitlab requires permissioned accounts, it is not necessary to protect it also behind a VPN.