# Important Links

### Gitlab Links

- [The repo for this documentation](https://gitlab.com/TransparentIncDevelopment/docs/tpfs-scrum-basics)

### External Team Resources

- [Product Team PRD Board](https://miro.com/app/board/o9J_lozxBm8=/) - Contains the working process for Product's creations of PRDs (Product Requirments Document).  **Do not modify**, but contains links to in-progress and approved PRDs for viewing.

### ADO Links

- [Team Cookie Monster Dashboard](https://dev.azure.com/transparentsystems/xand-network/_dashboards/dashboard/0629de50-5111-46e0-8f2b-1230eab2fa2c) - Team dashboard which collects useful metrics and links.  Use this to track velocity, unplanned work, and turnaround time for the team.  **Feel free to edit this**.

- [Team Cookie Monster Backlog Board](https://dev.azure.com/transparentsystems/xand-network/_boards/board/t/Cookie%20Monster/Backlog%20items?showParents=false) - Filter this for @currentiteration to create a board view of the current sprint.

- [Team Cookie Monster Epics Backlog View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Epics/) - List of Epics the team is tracking for completion.  **Keep this up to date and prioritized**. 

- [Team Cookie Monster Features Backlog View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Features/) - **Keep this up to date and prioritized.**  

- [Team Cookie Monster Backlog Item View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Backlog%20items/) - List of backlog items which must be parented to features and epics.  **Keep this up to date and prioritized**.  Keeping the first 50-ish items prioritized is of higher value than continuously pruning the entire backlog, though.

- [ADO Global Iteration Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work) - This is where iterations are created (they must also be assigned to teams!).  **Make sure that a good number of future iterations are created under V1**.

- [Team Cookie Monster Iteration Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work-team?_a=iterations&teamId=30fbba25-fea0-4862-a036-eebab482fc5a) - This is where iterations are created (they must also be assigned to teams!).  **Make sure that a good number of future iterations are created under V1**.