# Mandates

The following are decisions made about the Scrum Role by the org and this document serves as a source of truth about the state of these decisions.  Modifications to this section must be made for a change to be considered in effect.

- The general list of priorities is as follows:
    - PRD-derived work
    - Bugs
    - Any SGD followup work
    - Any Retrospective followup work
    - Technical Debt
- Technical debt is understood to be about 10% of the sprint's effort
- Unplanned work is not estimated, but the impact is measured through a lower velocity and via a dashboard metric
- Work must not be taken on without acceptance criteria defined and an estimate given
