# Responsibilities

>**NOTE:** Scrum Master is a hybrid role you share with your engineering responsibilities.  As such, make sure that you practice time management when handling both domains.  Delegate when possible, with volunteers first and then deputized collegues second.  As Scrum Master, you *are not team lead*, you are a facilitator for the team to stay organized and tracking progress together.  Collaborate as much as possible and document as much as possible for others to follow your paper trails.

The following are what you are accountable for as Scrum Master:

### Backlog Management

See ["Backlog Management"](#backlog-management-1) for process instructions.

- Keep the epics, features, and backlog items prioritized (they are prioritized separately in ADO).
- Make sure that each product backlog item / bug has a feature or epic parent.
- Keep item tags (`needs discussion`, `needs priority`, etc) up to date on all ADO items for team members.
- Delegate to team members for getting unprioritized items on the board with a `needs priority` tag.

### Estimations

See ["Estimations"](#estimations-1) for process instructions.

- Make sure that all upcoming items are estimated by the team via ADO or synchronous meeting.  
- Do not estimate too far in advance of when the work will be taken.
- Use the dashboard and prior estimations to predict reasonable point velocities for sprints.


### PRD (Product Requirement Document) Handling

See ["New Feature Process"](#new-feature-process) for process instructions.

- Coordinate with Product to keep up to date on PRD statuses and any priority changes.
- When PRDs are given final approval, facilitate the design and effort planning process.
- Once a feature is designed and the effort planned, the effort should be prioritized in the backlog against other items.


### Team Facilitation

- Run standup meetings and retrospectives
- Follow up on all takeaway action items from any meeting (e.g. retrospectives) with a paper trail + properly organized ADO items
- Take note of any frustrations or road blocks the team is facing and talk 1:1 if necessary to gather data, then plan or delegate followup actions to alleviate what you can
- Keep all Outlook schedules with the team up to date