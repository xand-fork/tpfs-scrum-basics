# Backlog Management

There are two big goals to backlog management:

- Make sure the team knows what they're supposed to work on and how to find that work
- Make sure that the work the team is taking on is properly prioritized against business and engineering interests

The team will want to add bugs and product backlog items or features to the backlog at certain points.  It will also be the Scrum Master's job to help them do that by following the guidance here.

### How to create new sprint iterations

ADO Assigns iterations from a schedule that is configured at the global level [here](https://dev.azure.com/transparentsystems/xand-network/_settings/work).

- Create a `New Child` under the V1 iteration group (which we have been using until the V1 release)
- Name the iteration for the year and week the iteration starts.  e.g., `22.17` starts the 17th week.   Iterations are odd-numbered going up by 2 each time, usually.
- Choose a monday start date for the sprint and ADO will automatically create a 2-week end date
- The ADO UI allows you to make multiple child iterations at once!

 ![](assets/new-v1-iteration.png "New Child Iteration Under V1")

 ![](assets/new-iteration-dialog.png "Example New Iteration")


 After the iteration is created, the team needs to be assigned to the new iterations [here](https://dev.azure.com/transparentsystems/xand-network/_settings/work)

 - Click `Select Iteration(s)`
 - Choose any number of globally-created iterations from the dropdown (e.g. `V1\22.17`)
 - Confirm the choice

  ![](assets/new-team-iterations.png "New Team Iterations")

 Once this is done ADO will detect which iteration it is in automatically, and anyone will be able to assign items to the sprints in the story editor UI.


### ADO Item Hierarchy

When the Scrum Master or any other person makes a new story or bug, they should ensure that it is parented to a tracking feature or epic.  If one does not exist, it should be created.  Typically, work that will stem multiple features, like work that comes from delivered PRDs, or IT work, will take multiple features and require an Epic to group them.  

Make sure that there are no "floating" backlog items or bugs and that each one has a feature or epic parent.  Those are easy to lose track of!  If a team member makes an item and it has no parent, make sure it is parented appropriately.

### Important Tags

The following are tags that should be added to epics, features, and backlog items/bugs as soon as they meet the criteria:

- `needs discussion` - This item is speculative and may or may not be necessary.  It requires team discussion to see if it shoudl be adopted.
- `needs discovery` - This item requires spiking before it can be taken on. Link the spike as a predecessor if one exists.
- `needs ac` - This item requires Acceptance Criteria 
- `needs priority` - This item was newly added (or unprioritized) and hasn't been prioritized yet.  These are high priority items to tackle during a grooming session and the team should be used to adding these to new items they create.
- `blocked` - This item has a finish-to-start constraint with a predecessor or has an external depdendency that hasn't resolved. 
- `needs estimation` / `needs timebox` - This item or spike needs an estimation from the team.
- `unplanned` - This item is unplanned product support work and shouldn't be estimated but is high priority

Make sure that the **Iteration path** of every item is kept up to date so that items that were placed in older sprints don't get lost.

### Prioritization

Each level of items -- Epic, Feature, and Backlog Item/Bug -- must be prioritized in the backlog separately.  Prioritize the epics first, then prioritize the features that are children of those epics.  Finally, prioritize the backlog items according to the priorities of the previous lists.

Prioritizing the backlog items / bugs is the most time consuming, and should be done in a grooming session instead of ad-hoc.  It is only important to keep a prioritized backlog for reasonably upcoming work, so if the backlog is hundreds of items long then prioritizing only the epics / features and then the first 50 or 100 most important items from those would be sufficient, as long as all items are still kept track of to make sure no important ones are left unprioritized.

### Adding new stories / bugs

If a new product backlog item needs to be created by a team member, it should be properly parented, tagged with `needs priority`, and the Scrum Master(s) alerted so it can be properly triaged.

If a new bug needs to be created, that bug should be added to either the feature the bug was found during, or -- if that doesn't apply -- the `Bugs` Epic for the team.
