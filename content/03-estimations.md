# Estimations

Each story needs to be estimated before being taken on, and each spike needs to be timeboxed beforehand.  This will mean either an ad-hoc estimation with the team during the story's creation, or creating an estimation session in ADO and then meeting with the team to come to a consensus on the score later.

### When should you create an estimation session?

You should create an estimation session if there is work that is coming up within the next couple of sprints that has the tag `needs estimation`.  If work is created from a design process, that work should be estimated ASAP after being created in ADO.  Ad-hoc work that is added over time should be estimated if it is prioritized highly.

### How to set up an estimation session

- In a backlog view, select all product backlog items you want to be in the same estimation session
- Click the triple-dot icon on the right side of any of the items
- Select "Estimate Work Items"

 ![](assets/starting-estimation.png "Starting an Estimation")

- When setting up the session, choose a good name
- Select "Offline" for the type, so that votes are saved over time
- Choose the "Fibonacci" card sequence
- Click "Create" to make the session

 ![](assets/estimation-settings.png "Starting an Estimation")

 When the session has finished, choose the triple-dot icon at the top of the session and "End Session" to remove it from the list of active estimation sessions.

 ![](assets/end-session.png "Ending an Estimation Session")
