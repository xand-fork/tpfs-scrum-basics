# New Feature Process

New product features follow a process instead of being added to the backlog ad-hoc.  

If a product feature is coming from the Product team and a PRD, it's likely that multiple ADO feature items will be required and an Epic should be used to track the item.  If a team member wants to add a single feature for a collection of backlog items, add it to an existing epic if appropriate, or make a new one for it.

It's important to note that this process separates each step into its own deliverable.  Taking requirements from a PRD and putting them in an ADO story is not sufficient and will lead to technical debt.

[Here](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Features/?showParents=false&workitem=7334) is an example of an item which was given a PRD, then designed and the effort planned.

### Definition

First, the feature must be defined.  If this is a feature from a non-engineering party such as Product, it requires a PRD that is approved by the head of engineering.  If it is a single ADO feature from a team member it must contain a full description of the problem and requirements of any solution implemented.

### Design + Approval

Once there are requirements defined for the work, the team should take 2-3 volunteers to architect a solution which fulfilles those requirements.  Depending on the complexity of the problem, a proper design can be as complex as a diagram on Miro or as simple as a written explanation of what change needs to occur.  The output of the design process must be sufficient for other engineers to understand the design of the solution and have intelligent discussions about implementation details.  

Once drafted, a design must be approved by a principal engineer.  After that approval, the feature is officially designed and effort can be planned.

### Effort Planning + Prioritization

After the design is approved, volunteers from the team should meet and plan the effort for the implementation of the design.  That means breaking the design into ADO feature and backlog item sets which are reasonably scoped with descriptions and acceptance criteria.

Here are a few tips on doing that:

1. Try to keep effort parallelizable so that the work can be accomplished simultaneously
1. Track the predecessor and successor of every story and make sure to block dependent items which have a finish-to-start constraint.  A solid papertrail is important!
1. The definition of done for a feature includes updating our deployments and tests

[This Miro board](https://miro.com/app/board/o9J_lENK6pU=/) contains examples of the effort planning for Bank Transaction Repository.  (under "planning" on the right side of the board).

### Implementation

When all the items are in ADO, they should be prioritized by the Scrum Masters and estimated.