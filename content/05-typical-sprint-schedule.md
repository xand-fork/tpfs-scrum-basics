# Typical Sprint Schedule

|   MON	|   TUE	|   WED	|  THUR	|   FRI	|
|:---	|:---	|:---	|:---	|:---	|
|   **1) Begin the sprint**<br>2) Sync with product on any outstanding items	|  General duties<br>(This is a good time to start working on any last retrospective followups you can help with)	|  General duties	| General duties 	|   1) Major Backlog Grooming<br>Session w/ Co-Scrum Leader	|
|  General duties	|   1) Next Sprint Planning w/ Team<br>2) Good time to second-pass prioritize	|  General duties	| 1) Sync with Lead about Sprint<br>2) Facilitate Retrospective and Create Followup Items 	|   **1) End the sprint**	|
