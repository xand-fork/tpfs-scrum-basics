# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script generates a single HTML file from the markdown content in this project.

    Options
      -h: Print help
END
  )"
  echo -e "$HELPTEXT"
}

# Args parsing pulled from last post here: https://www.unix.com/shell-programming-and-scripting/59607-getopts-alternative.html
while true
do
  case $# in 0) break ;; esac
  case $1 in
    -h|--help) print_help && exit 0 ;;
    -*) echo "Invalid option $1" && print_help && exit 1 ;;
    *) break ;;
  esac
done

# SELF
DIR="$( dirname pwd )"
ABS_PATH_DIR="$(realpath ${DIR})"
ENV_DIR="${ABS_PATH_DIR}/build.env"
TMP="${ABS_PATH_DIR}/tmp" # Volume-mount target for pandoc docker container

PANDOC_LATEX_DOCKER_IMAGE="pandoc/latex:2.14.0.2"

# Variables for building input paths
ASSETS_DIR_NAME="assets"
CONTENT_DIR_NAME="content"
STYLE_DIR_NAME="style"
STYLE_REL_PATH=${STYLE_DIR_NAME}/style.css
HTML_TEMPLATE_REL_PATH=${STYLE_DIR_NAME}/pandoc-template.html5

# Variables for building output paths and filenames
OUTPUT_DIR_NAME="build"
HTML_OUTPUT="README.html"

# CONSTRUCTED SETTINGS
ASSETS_ABS_PATH="${ABS_PATH_DIR}/${ASSETS_DIR_NAME}"
CONTENT_ABS_PATH_DIR="${ABS_PATH_DIR}/${CONTENT_DIR_NAME}"
STYLE_ABS_PATH="${ABS_PATH_DIR}/${STYLE_DIR_NAME}"

# Get list of markdown files within content/* dir
# `-printf "%P\n"` prints the filename relative to CONTENT_ABS_PATH_DIR
DOCS=$(find "${CONTENT_ABS_PATH_DIR}" -type f -name "*.md" -printf "${CONTENT_DIR_NAME}/%P\n" | sort)

OUTPUT_DIR="${ABS_PATH_DIR}/${OUTPUT_DIR_NAME}"

create_build_dirs() {
  rm -rf ${OUTPUT_DIR}/*
  rm -rf ${TMP}
  mkdir -p ${OUTPUT_DIR}
}

move_temp_files() {
  mkdir -p ${TMP}
  cp -r ${CONTENT_ABS_PATH_DIR} ${TMP}
  cp -r ${STYLE_ABS_PATH} ${TMP}
}

generate_md_template() {
  echo -e "Generating markdown template from docs..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --wrap preserve \
      --to="gfm" \
      --from="gfm" \
      -o ${OUTPUT_DIR}/README.md.template \
      ${DOCS}
  echo -e "Done."
}

generate_md() {
  echo -e "Generating markdown from markdown template..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --wrap preserve \
      --template=${OUTPUT_DIR}/README.md.template \
      --to="gfm" \
      --from="gfm" \
     -o ${OUTPUT_DIR}/README.md \
     /dev/null
  echo -e "Done."
}

generate_html_from_markdown() {
  echo -e "Generating HTML from markdown..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --from=gfm \
      --standalone \
      --resource-path ${ASSETS_DIR_NAME} \
      --toc \
      --toc-depth 4 \
      -V block-headings \
      --self-contained \
      --metadata title="TPFS Scrum Basics" \
      --section-divs \
      --css=${STYLE_REL_PATH} \
      --template=${HTML_TEMPLATE_REL_PATH} \
      -o "${OUTPUT_DIR}/README.html" \
      ${OUTPUT_DIR}/README.md
  echo -e "Done."
}

clean_temp() {
  rm -rf ${TMP}
}

clean_temp
create_build_dirs
move_temp_files
generate_md_template
generate_md
generate_html_from_markdown
clean_temp

echo -e "Finished building Documentation."